package com.jceceniceros.ribbit.ui;

import android.app.Application;

import com.jceceniceros.ribbit.classes.ParseConstants;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

/**
 * Created by juancarlos on 14/05/15.
 */
public class RibbitApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "m4LA9Rt9otvPHiH7NsMbvfO9OHNUsjgTA5Qsm4Vh", "IFZ0mDdyI1vBNxzWVV5V5qYvrjdedzeM6d34KsbD");
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }

    public static void updateParseInstallation(ParseUser user) {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put(ParseConstants.KEY_USER_ID, user.getObjectId());
        installation.saveInBackground();
    }
}
