package com.jceceniceros.ribbit.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.jceceniceros.ribbit.R;
import com.jceceniceros.ribbit.adapters.SectionsPagerAdapter;
import com.jceceniceros.ribbit.classes.ParseConstants;
import com.jceceniceros.ribbit.ui.widgets.SlidingTabLayout;
import com.parse.ParseUser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int TAKE_PHOTO_REQUEST = 0;
    private static final int TAKE_VIDEO_REQUEST = 1;
    private static final int PICK_PHOTO_REQUEST = 2;
    private static final int PICK_VIDEO_REQUEST = 3;
    private static final int MEDIA_TYPE_IMAGE = 4;
    private static final int MEDIA_TYPE_VIDEO = 5;
    private static final int FILE_SIZE_LIMIT = 1024*1024*5;

    private Uri mMediaUri;

    SectionsPagerAdapter mSectionsPagerAdapter;

    @InjectView(R.id.toolbar) Toolbar mToolbar;
    @InjectView(R.id.tabs) SlidingTabLayout mTabs;
    @InjectView(R.id.pager) ViewPager mViewPager;

    private DialogInterface.OnClickListener mDialogListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch(which) {
                case 0:
                    launchTakePhotoIntent();
                    break;
                case 1:
                    launchTakeVideoIntent();
                    break;
                case 2:
                    launchPickPhotoIntent();
                    break;
                case 3:
                    launchPickVideoIntent();
                    break;
            }
        }

        private void launchTakePhotoIntent() {
            Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            mMediaUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            if(mMediaUri == null) {
                Toast.makeText(MainActivity.this, getString(R.string.error_external_storage), Toast.LENGTH_LONG).show();
            } else {
                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, mMediaUri);
                startActivityForResult(takePhotoIntent, TAKE_PHOTO_REQUEST);
            }
        }

        private void launchTakeVideoIntent() {
            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            mMediaUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
            if(mMediaUri == null) {
                Toast.makeText(MainActivity.this, getString(R.string.error_external_storage), Toast.LENGTH_LONG).show();
            } else {
                takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, mMediaUri);
                takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
                takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
                startActivityForResult(takeVideoIntent, TAKE_VIDEO_REQUEST);
            }
        }

        private void launchPickPhotoIntent() {
            Intent pickPhotoIntent = new Intent(Intent.ACTION_GET_CONTENT);
            pickPhotoIntent.setType("image/*");
            startActivityForResult(pickPhotoIntent, PICK_PHOTO_REQUEST);
        }

        private void launchPickVideoIntent() {
            Intent pickVideoIntent = new Intent(Intent.ACTION_GET_CONTENT);
            pickVideoIntent.setType("video/*");
            Toast.makeText(MainActivity.this, getString(R.string.warning_file_size), Toast.LENGTH_LONG).show();
            startActivityForResult(pickVideoIntent, PICK_VIDEO_REQUEST);
        }

        private Uri getOutputMediaFileUri(int mediaType) {
            // To be safe, you should check that the SDCard is mounted
            // using Environment.getExternalStorageState() before doing this.
            if(isExternalStorageAvailable()) {

                // Get external storage directory
                String appName = MainActivity.this.getString(R.string.app_name);
                File mediaStorageDir = new File(
                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                        appName
                );

                // Create subdirectory
                if (!mediaStorageDir.exists()){
                    if (!mediaStorageDir.mkdirs()){
                        Log.d(TAG, "Failed to create directory");
                        return null;
                    }
                }

                // Create filename
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
                File mediaFile;
                String path = mediaStorageDir.getPath() + File.separator;

                if (mediaType == MEDIA_TYPE_IMAGE){
                    mediaFile = new File(path + "IMG_"+ timeStamp + ".jpg");
                } else if(mediaType == MEDIA_TYPE_VIDEO) {
                    mediaFile = new File(path + "VID_"+ timeStamp + ".mp4");
                } else {
                    return null;
                }

                Log.d(TAG, "File " + Uri.fromFile(mediaFile));

                return Uri.fromFile(mediaFile);
            } else {
                return null;
            }
        }

        private boolean isExternalStorageAvailable() {
            return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null) {
            Log.i(TAG, currentUser.getUsername());
        } else {
            toLogin();
        }

        // Set up the action bar.
        mToolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(mToolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // Assigning the Sliding Tab Layout View
        mTabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        mTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabs_scroll_color);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        mTabs.setViewPager(mViewPager);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK) {
            if(requestCode == PICK_PHOTO_REQUEST || requestCode == PICK_VIDEO_REQUEST) {

                if(data == null) {
                    Toast.makeText(MainActivity.this, getString(R.string.general_error), Toast.LENGTH_LONG).show();
                    return;
                } else {
                    mMediaUri = data.getData();
                    /*
                    if(requestCode == PICK_PHOTO_REQUEST) {
                        this.getContentResolver().delete(data.getData(), null, null);
                    }
                    */
                }

                Log.i(TAG, "Media URI: " + mMediaUri);

                // Check file size
                //if(requestCode == PICK_VIDEO_REQUEST) {
                int fileSize = 0;
                InputStream inputStream = null;
                try {
                    inputStream = getContentResolver().openInputStream(mMediaUri);
                    fileSize = inputStream.available();
                } catch (FileNotFoundException e) {
                    //e.printStackTrace();
                    Toast.makeText(MainActivity.this, getString(R.string.error_opening_file), Toast.LENGTH_LONG).show();
                    return;
                } catch (IOException e) {
                    //e.printStackTrace();
                    Toast.makeText(MainActivity.this, getString(R.string.error_opening_file), Toast.LENGTH_LONG).show();
                    return;
                } finally {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        //e.printStackTrace();
                        /* Intentionally in blank */
                    }
                }

                Log.i(TAG, "File size: " + fileSize);

                if(fileSize >= FILE_SIZE_LIMIT) {
                    Toast.makeText(MainActivity.this, getString(R.string.error_file_size_large), Toast.LENGTH_LONG).show();
                }
                //}
            } else {
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                mediaScanIntent.setData(mMediaUri);
                sendBroadcast(mediaScanIntent);
            }

            Intent recipientsIntent = new Intent(this, RecipientsActivity.class);
            String fileType;
            if(requestCode == PICK_PHOTO_REQUEST || requestCode == TAKE_PHOTO_REQUEST) {
                fileType = ParseConstants.TYPE_IMAGE;
            } else {
                fileType = ParseConstants.TYPE_VIDEO;
            }

            recipientsIntent.setData(mMediaUri);
            recipientsIntent.putExtra(ParseConstants.KEY_FILE_TYPE, fileType);
            recipientsIntent.putExtra(ParseConstants.KEY_MESSAGE_TYPE, "media");
            startActivity(recipientsIntent);
        } else if(requestCode != RESULT_CANCELED) {
            Toast.makeText(MainActivity.this, getString(R.string.general_error), Toast.LENGTH_LONG).show();
        }
    }

    private void toLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch(id){
            case R.id.action_logout:
                actionLogout();
                break;
            case R.id.action_edit_friends:
                actionEditFriends();
                break;
            case R.id.action_camera:
                actionCamera();
                break;
            case R.id.action_message:
                actionMessage();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void actionLogout() {
        ParseUser.logOut();
        toLogin();
    }

    private void actionEditFriends() {
        Intent intent = new Intent(this, EditFriendsActivity.class);
        startActivity(intent);
    }

    private void actionCamera() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(R.array.camera_choices, mDialogListener);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void actionMessage() {
        Toast.makeText(MainActivity.this, getString(R.string.warning_message_size), Toast.LENGTH_LONG).show();

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("New message"); //Set Alert dialog title here
        alert.setMessage("Write your message:"); //Message here

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("SEND", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String message = input.getText().toString();

                if(message.length() != 0) {
                    if(message.length() > 150) {
                        message = message.substring(0, 150);
                    }
                    Intent recipientsIntent = new Intent(MainActivity.this, RecipientsActivity.class);
                    recipientsIntent.putExtra(ParseConstants.KEY_MESSAGE_TYPE, "text");
                    recipientsIntent.putExtra(ParseConstants.KEY_FILE_TYPE, "text");
                    recipientsIntent.putExtra(ParseConstants.KEY_TEXT, message);
                    startActivity(recipientsIntent);
                } else {
                    Toast.makeText(MainActivity.this, getString(R.string.warning_message_size), Toast.LENGTH_LONG).show();
                    dialog.cancel();
                }
            }
        });

        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }
}
