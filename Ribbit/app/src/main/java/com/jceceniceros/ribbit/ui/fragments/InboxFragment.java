package com.jceceniceros.ribbit.ui.fragments;

/**
 * Created by juancarlos on 16/05/15.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.jceceniceros.ribbit.R;
import com.jceceniceros.ribbit.adapters.MessageAdapter;
import com.jceceniceros.ribbit.classes.ParseConstants;
import com.jceceniceros.ribbit.helpers.FileHelper;
import com.jceceniceros.ribbit.ui.ViewImageActivity;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class InboxFragment extends ListFragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String TAG = InboxFragment.class.getSimpleName();

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private List<ParseObject> mMessages;


    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static InboxFragment newInstance(int sectionNumber) {
        InboxFragment fragment = new InboxFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public InboxFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_inbox, container, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);
        mSwipeRefreshLayout.setColorSchemeColors(
                R.color.swipeRefreshColor1,
                R.color.swipeRefreshColor2,
                R.color.swipeRefreshColor3,
                R.color.swipeRefreshColor4);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        retrieveMessages();

    }

    private void retrieveMessages() {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(ParseConstants.CLASS_MESSAGES);
        query.whereEqualTo(ParseConstants.KEY_RECIPIENTS_IDS, ParseUser.getCurrentUser().getObjectId());
        query.addDescendingOrder(ParseConstants.KEY_CREATED_AT);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> messages, ParseException e) {
                if(mSwipeRefreshLayout.isRefreshing()){
                    mSwipeRefreshLayout.setRefreshing(false);
                }
                if (e == null) {
                    mMessages = messages;

                    if (getListView().getAdapter() == null) {
                        MessageAdapter adapter = new MessageAdapter(getListView().getContext(), mMessages);
                        getListView().setAdapter(adapter);
                    } else {
                        ((MessageAdapter) getListView().getAdapter()).refill(mMessages);
                    }
                }
            }
        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        ParseObject message = mMessages.get(position);
        String messageType = message.getString(ParseConstants.KEY_FILE_TYPE);
        ParseFile file;
        Uri fileUri = null;
        String text = "Placeholder";

        Toast.makeText(getActivity(), getString(R.string.warning_destroy_message), Toast.LENGTH_LONG).show();
        
        if(messageType.equals(ParseConstants.TYPE_TEXT)){
            text = message.getString(ParseConstants.KEY_TEXT);
        } else {
            file = message.getParseFile(ParseConstants.KEY_FILE);
            fileUri = Uri.parse(file.getUrl());
        }

        Intent intent;
        switch(messageType){
            case ParseConstants.TYPE_IMAGE:
                intent = new Intent(getActivity(), ViewImageActivity.class);
                intent.setData(fileUri);
                startActivity(intent);
                break;
            case ParseConstants.TYPE_VIDEO:
                intent = new Intent(Intent.ACTION_VIEW, fileUri);
                intent.setDataAndType(fileUri, "video/*");
                startActivity(intent);
                break;
            case ParseConstants.TYPE_TEXT:
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("From: " + message.getString(ParseConstants.KEY_SENDER_NAME));
                alert.setMessage(text); //Message here

                //alert.setPositiveButton("OK", null);

                alert.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alert.create();
                alertDialog.show();
                break;
        }

        List<String> ids = message.getList(ParseConstants.KEY_RECIPIENTS_IDS);
        if(ids.size() == 1) {
            message.deleteInBackground();
            if(!messageType.equals(ParseConstants.TYPE_TEXT)) {
                deleteFile(fileUri);
            }
        } else {
            ids.remove(ParseUser.getCurrentUser().getObjectId());
            ArrayList<String> idsToRemove = new ArrayList<String>();
            idsToRemove.add(ParseUser.getCurrentUser().getObjectId());
            message.removeAll(ParseConstants.KEY_RECIPIENTS_IDS, idsToRemove);
            message.saveInBackground();
        }
    }

    private void deleteFile(Uri fileUri) {
        String endpoint = "https://api.parse.com/1/files/";
        String filename = FileHelper.getFileNameFromUrl(fileUri.toString());
        String url = endpoint + filename;

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url)
                                               .header("X-Parse-Application-Id", "m4LA9Rt9otvPHiH7NsMbvfO9OHNUsjgTA5Qsm4Vh")
                                               .header("X-Parse-Master-Key", "vh3zmEjy9qOIzGAnpe5SsVcwgQNlayfRQRKj4kb0")
                                               .delete()
                                               .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try {
                    String jsonData = response.body().string();
                    Log.v(TAG, jsonData);
                    if (response.isSuccessful()) {
                        //Toast.makeText(getActivity(), "File deletion success.", Toast.LENGTH_LONG).show();
                    } else {
                        //Toast.makeText(getActivity(), "File deletion success, but bad response.", Toast.LENGTH_LONG).show();
                        throw new IOException("Unexpected code " + response);
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Exception caught: ", e);
                }
            }
        });
    }

    private OnRefreshListener mOnRefreshListener = new OnRefreshListener() {
        @Override
        public void onRefresh() {
            retrieveMessages();
        }
    };
}
