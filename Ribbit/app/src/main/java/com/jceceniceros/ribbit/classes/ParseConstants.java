package com.jceceniceros.ribbit.classes;

/**
 * Created by juancarlos on 17/05/15.
 */
public final class ParseConstants {
    // Class name
    public static final String CLASS_MESSAGES = "Messages";

    // Key names
    public static final String KEY_USER_ID = "userId";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_FRIENDS_RELATION = "friends_relation";
    public static final String KEY_MESSAGE_TYPE = "messageType";
    public static final String KEY_RECIPIENTS_IDS = "recipientsIds";
    public static final String KEY_SENDER_ID = "senderId";
    public static final String KEY_SENDER_NAME = "senderName";
    public static final String KEY_TEXT = "text";
    public static final String KEY_FILE = "file";
    public static final String KEY_FILE_TYPE = "fileType";
    public static final String KEY_CREATED_AT = "createdAt";

    // Types
    public static final String TYPE_IMAGE = "image";
    public static final String TYPE_TEXT = "text";
    public static final String TYPE_VIDEO = "video";
}
