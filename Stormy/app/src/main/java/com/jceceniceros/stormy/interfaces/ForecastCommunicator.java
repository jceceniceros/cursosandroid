package com.jceceniceros.stormy.interfaces;

/**
 * Created by juancarlos on 25/05/15.
 */
public interface ForecastCommunicator {
    public void onForecastUpdate(String jsonData);
}
