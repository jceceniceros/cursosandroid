package com.jceceniceros.stormy.ui.fragments;

/**
 * Created by juancarlos on 16/05/15.
 */

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jceceniceros.stormy.R;
import com.jceceniceros.stormy.adapters.HourAdapter;
import com.jceceniceros.stormy.interfaces.ForecastCommunicator;
import com.jceceniceros.stormy.interfaces.HourlyForecastCommunicator;
import com.jceceniceros.stormy.ui.MainActivity;
import com.jceceniceros.stormy.weather.Hour;

/**
 * A placeholder fragment containing a simple view.
 */
public class HourlyForecastFragment extends Fragment implements HourlyForecastCommunicator {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String TAG = HourlyForecastFragment.class.getSimpleName();

    private ForecastCommunicator mForecastCommunicator;
    private Hour[] mHours;
    private RecyclerView mRecyclerView;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static HourlyForecastFragment newInstance(int sectionNumber) {
        HourlyForecastFragment fragment = new HourlyForecastFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public HourlyForecastFragment() {
    }

    public void setAdapter() {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(mRecyclerView.getAdapter() == null) {
                        HourAdapter adapter = new HourAdapter(getActivity(), mHours);
                        mRecyclerView.setAdapter(adapter);
                    } else {
                        ((HourAdapter) mRecyclerView.getAdapter()).refill(mHours);
                    }
                }
            });
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mForecastCommunicator = (ForecastCommunicator) activity;
            ((MainActivity) activity).mHourlyForecastCommunicator = this;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement HourlyForecastCommunicator");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_hourly_forecast, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mForecastCommunicator.onForecastUpdate("hours");

        setAdapter();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
    }

    @Override
    public void onHourlyForecastUpdate(Hour[] hours) {
        mHours = hours;
        setAdapter();
    }
}
