package com.jceceniceros.stormy.ui.fragments;

/**
 * Created by juancarlos on 16/05/15.
 */

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jceceniceros.stormy.R;
import com.jceceniceros.stormy.interfaces.CurrentForecastCommunicator;
import com.jceceniceros.stormy.interfaces.ForecastCommunicator;
import com.jceceniceros.stormy.ui.MainActivity;
import com.jceceniceros.stormy.weather.Current;

/**
 * A placeholder fragment containing a simple view.
 */
public class CurrentForecastFragment extends Fragment implements CurrentForecastCommunicator {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String TAG = CurrentForecastFragment.class.getSimpleName();

    private ForecastCommunicator mForecastCommunicator;
    private Current mCurrent;
    private View mView;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static CurrentForecastFragment newInstance(int sectionNumber) {
        CurrentForecastFragment fragment = new CurrentForecastFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public CurrentForecastFragment() {
    }

    public void updateUI() {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((TextView) mView.findViewById(R.id.temperatureLabel)).setText(mCurrent.getTemperature() + "");
                    ((TextView) mView.findViewById(R.id.timeLabel)).setText("at " + mCurrent.getFormattedTime() + " it will be");
                    ((TextView) mView.findViewById(R.id.humidityValue)).setText(mCurrent.getHumidity() + "");
                    ((TextView) mView.findViewById(R.id.precipValue)).setText(mCurrent.getPrecipChance() + "%");
                    ((TextView) mView.findViewById(R.id.summaryLabel)).setText(mCurrent.getSummary());
                    Drawable drawable = getResources().getDrawable(mCurrent.getIconId());
                    ((ImageView) mView.findViewById(R.id.iconImageView)).setImageDrawable(drawable);
                }
            });
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mForecastCommunicator = (ForecastCommunicator) activity;
            ((MainActivity) activity).mCurrentForecastCommunicator = this;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement DailyForecastCommunicator");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_today_forecast, container, false);
        mView = rootView;
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mForecastCommunicator.onForecastUpdate("current");
        updateUI();
    }

    @Override
    public void onCurrentForecastUpdate(Current current) {
        mCurrent = current;
        updateUI();
    }
}
