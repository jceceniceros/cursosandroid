package com.jceceniceros.stormy.ui.fragments;

/**
 * Created by juancarlos on 16/05/15.
 */

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.jceceniceros.stormy.R;
import com.jceceniceros.stormy.adapters.DayAdapter;
import com.jceceniceros.stormy.interfaces.DailyForecastCommunicator;
import com.jceceniceros.stormy.interfaces.ForecastCommunicator;
import com.jceceniceros.stormy.ui.MainActivity;
import com.jceceniceros.stormy.weather.Day;

import butterknife.InjectView;

/**
 * A placeholder fragment containing a simple view.
 */
public class DailyForecastFragment extends ListFragment implements DailyForecastCommunicator {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String TAG = DailyForecastFragment.class.getSimpleName();

    private ForecastCommunicator mForecastCommunicator;
    private Day[] mDays;

    @InjectView(android.R.id.empty) TextView mEmptyTextView;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static DailyForecastFragment newInstance(int sectionNumber) {
        DailyForecastFragment fragment = new DailyForecastFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public DailyForecastFragment() {
    }

    private void setAdapter() {
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(getListView().getAdapter() == null) {
                        DayAdapter adapter= new DayAdapter(getActivity(), mDays);
                        getListView().setAdapter(adapter);
                    } else {
                        ((DayAdapter) getListView().getAdapter()).refill(mDays);
                    }
                }
            });
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mForecastCommunicator = (ForecastCommunicator) activity;
            ((MainActivity) activity).mDailyForecastCommunicator = this;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement DailyForecastCommunicator");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_daily_forecast, container, false);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mForecastCommunicator.onForecastUpdate("days");

        setAdapter();

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String dayOfTheWeek = mDays[position].getDayOfTheWeek();
                String conditions = mDays[position].getSummary();
                String message = String.format("%s: %s", dayOfTheWeek, conditions);
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onDailyForecastUpdate(Day[] days) {
        mDays = days;
        setAdapter();
    }
}
