package com.jceceniceros.stormy.interfaces;

import com.jceceniceros.stormy.weather.Day;

/**
 * Created by juancarlos on 25/05/15.
 */
public interface DailyForecastCommunicator {
    public void onDailyForecastUpdate(Day[] days);
}
