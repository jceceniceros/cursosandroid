package com.jceceniceros.stormy.interfaces;

import com.jceceniceros.stormy.weather.Hour;

/**
 * Created by juancarlos on 25/05/15.
 */
public interface HourlyForecastCommunicator {
    public void onHourlyForecastUpdate(Hour[] hours);
}
