package com.jceceniceros.stormy.interfaces;

import com.jceceniceros.stormy.weather.Current;

/**
 * Created by juancarlos on 25/05/15.
 */
public interface CurrentForecastCommunicator {
    public void onCurrentForecastUpdate(Current current);
}
