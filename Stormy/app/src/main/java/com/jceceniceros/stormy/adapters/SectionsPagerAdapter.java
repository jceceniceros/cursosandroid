package com.jceceniceros.stormy.adapters;

/**
 * Created by juancarlos on 16/05/15.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import com.jceceniceros.stormy.R;
import com.jceceniceros.stormy.ui.fragments.CurrentForecastFragment;
import com.jceceniceros.stormy.ui.fragments.DailyForecastFragment;
import com.jceceniceros.stormy.ui.fragments.HourlyForecastFragment;

import java.util.Locale;

/**
 * A {@link FragmentStatePagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    protected Context mContext;
    private SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        switch(position) {
            case 0:
                return HourlyForecastFragment.newInstance(position + 1);
            case 1:
                return CurrentForecastFragment.newInstance(position + 1);
            case 2:
                return DailyForecastFragment.newInstance(position + 1);
        }
        return null;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return mContext.getString(R.string.title_section1).toUpperCase(l);
            case 1:
                return mContext.getString(R.string.title_section2).toUpperCase(l);
            case 2:
                return mContext.getString(R.string.title_section3).toUpperCase(l);
        }
        return null;
    }
}
