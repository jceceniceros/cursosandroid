package com.jceceniceros.stormy.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jceceniceros.stormy.R;
import com.jceceniceros.stormy.weather.Day;

/**
 * Created by juancarlos on 24/04/15.
 */
public class DayAdapter extends BaseAdapter {

    private Context mContext;

    private Day[] mDays;

    public DayAdapter(Context context, Day[] days){
        mContext = context;
        mDays = days;
    }

    public void refill(Day[] days) {
        mDays = days;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mDays.length;
    }

    @Override
    public Object getItem(int position) {
        return mDays[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.daily_list_item, parent, false);
            holder = new ViewHolder();
            holder.iconImageView = (ImageView) convertView.findViewById(R.id.iconImageView);
            holder.minTemperatureLabel = (TextView) convertView.findViewById(R.id.temperatureLabel);
            holder.maxTemperatureLabel = (TextView) convertView.findViewById(R.id.maxTemperatureLabel);
            holder.dayLabel = (TextView) convertView.findViewById(R.id.dayNameLabel);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Day day = mDays[position];

        holder.iconImageView.setImageResource(day.getIconId());
        holder.minTemperatureLabel.setText(day.getTemperatureMin() + "");
        holder.maxTemperatureLabel.setText(day.getTemperatureMax() + "");

        if(position == 0) {
            holder.dayLabel.setText("Today");
        } else {
            holder.dayLabel.setText(day.getDayOfTheWeek());
        }

        return convertView;
    }

    private static class ViewHolder {
        ImageView iconImageView;
        TextView minTemperatureLabel;
        TextView maxTemperatureLabel;
        TextView dayLabel;
    }
}
